Source: mediawiki
Section: web
Priority: optional
Maintainer: Kunal Mehta <legoktm@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-buildinfo,
 apache2-dev
Homepage: https://www.mediawiki.org/
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/mediawiki-team/mediawiki.git
Vcs-Browser: https://salsa.debian.org/mediawiki-team/mediawiki
Rules-Requires-Root: no

Package: mediawiki
Architecture: all
Depends: apache2 | httpd,
 php (>= 2:7.3~),
 php-mysql | php-pgsql | php-sqlite3 | php-mysqlnd,
 php-mbstring,
 php-xml,
 mediawiki-classes (= ${binary:Version}),
 ${misc:Depends}
Recommends: default-mysql-server | virtual-mysql-server | postgresql-contrib | php-sqlite3,
 php-cli,
 php-apcu,
 php-intl,
 php-curl,
 php-gmp,
 php-wikidiff2,
 python3,
 php-luasandbox | lua5.1
Suggests: imagemagick | php-gd,
 memcached,
 clamav,
 firejail
Breaks: fusionforge-plugin-mediawiki (<< 5.2~rc1+1~),
 mediawiki-extensions-ldapauth (<< 2.8~),
 mediawiki-extensions-openid (<< 2.8~),
 mediawiki-extensions-fckeditor,
 mediawiki-extensions-collection (<< 2.8~),
 mediawiki-extensions-graphviz (<< 2.8~),
 mediawiki-extensions (<< 2.8~),
 mediawiki-extensions-base,
 mediawiki-extensions-confirmedit,
 mediawiki-extensions-geshi
Replaces: mediawiki-extensions-base,
 mediawiki-extensions-confirmedit,
 mediawiki-extensions-geshi
Provides: mediawiki-extensions-confirmedit,
 mediawiki-extensions-geshi
Description: website engine for collaborative work
 MediaWiki is a wiki engine (a program for creating a collaboratively
 edited website). It is designed to handle heavy websites containing
 library-like document collections, and supports user uploads of
 images/sounds, multilingual content, TOC autogeneration, ISBN links,
 etc.
 .
 Moreover, it keeps track of changes, so users can receive
 notifications, view diffs and revert edits. This system has many
 other features and can easily be extended.

Package: mediawiki-classes
Architecture: all
Depends: ${misc:Depends}
Description: website engine for collaborative work - standalone classes
 This package provides standalone classes from the remainder of the
 MediaWiki codebase. They do not call on any other portions of MediaWiki
 code, and can be used in portions of MediaWiki code, and can be used in
 other projects without dependency issues.
 .
 MediaWiki is a wiki engine (a program for creating a collaboratively
 edited website). It is designed to handle heavy websites containing
 library-like document collections, and supports user uploads of
 images/sounds, multilingual content, TOC autogeneration, ISBN links,
 etc.
 .
 Moreover, it keeps track of changes, so users can receive
 notifications, view diffs and revert edits. This system has many
 other features and can easily be extended.
